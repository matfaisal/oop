<?php
require('animal.php');
require('frog&ape.php');

$animal = new animal("Sheep");
echo $animal->name . "<br>";
echo $animal->legs ."<br>";
echo $animal->cold_blooded;


$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"